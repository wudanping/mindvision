# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""
 Produce the build model
"""

from .classifiers.base import BaseClassifier
from .backbones.lenet import Lenet5
from .backbones.mobilenet_v2 import MobileNetV2
from .neck.pooling import GlobalAvgPooling
from .head.linearclshead import LinearClsHead


def build_model(config):
    if config.model_name == "lenet5":
        net = BaseClassifier(backbone=Lenet5(num_class=config.classes_num), neck=None)
        return net
    elif config.model_name == "mobilenetv2":
        net = BaseClassifier(backbone=MobileNetV2(), neck=GlobalAvgPooling(), head=LinearClsHead())
        return net
    print("dont have this model")
    return None
