# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
""" MindVison Classification export script. """

import numpy as np
import mindspore
from mindspore import Tensor, context, load_checkpoint, load_param_into_net, export

from mindvision.classification.utils import get_config, parse_args
from mindvision.classification.models.build_train import build_model


def main(pargs):
    # set config context
    config = get_config(pargs.config, overrides=pargs.override)
    context.set_context(mode=context.GRAPH_MODE,
                        device_target=config.device_target)

    # set network, loss, optimizer
    network = build_model(config)

    # load pretrain model
    param_dict = load_checkpoint(config.pretrained_model)
    load_param_into_net(network, param_dict)

    # export network
    inputs = Tensor(np.ones([config.TRAIN.batch_size, 1, config.image_height, config.image_width]), mindspore.float32)
    export(network, inputs, file_name=config.EXPORT.file_name, file_format=config.EXPORT.file_format)
    print(f'[End of export `{config.model_name}`]')


if __name__ == '__main__':
    args = parse_args()
    main(args)

