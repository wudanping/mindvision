# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
'''Loading dataset.'''

import cv2
import mindspore.dataset as de
from mindspore.dataset import MindDataset as _MindDataset
from utils.class_factory import ClassFactory, ModuleType

@ClassFactory.register(ModuleType.DATASET)
class LoadMindDataset(_MindDataset):
    '''loading mindrecord dataset'''
    def __init__(self, mindrecord_file, columns_list, num_shards, rank_id, num_parallel_workers, is_training,
                 num_threads=0, prefetch_size=0):
        if not isinstance(num_threads, int):
            raise TypeError(f'num_threads must be a int, but got {type(num_threads)}')
        if not isinstance(prefetch_size, int):
            raise TypeError(f'prefetch_size must be a int, but got {type(prefetch_size)}')
        if num_threads >= 0:
            cv2.setNumThreads(num_threads)
        if prefetch_size > 0:
            de.config.set_prefetch_size(prefetch_size)
        super().__init__(mindrecord_file, columns_list=columns_list,
                         num_shards=num_shards, shard_id=rank_id,
                         num_parallel_workers=num_parallel_workers, shuffle=is_training)
