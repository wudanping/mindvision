# mindvision-pose

## Introduction

MindVision-Pose is an open-source toolbox for pose estimation based on MindSpore. It is a part of the MindVision project.

The master branch develop on **MindSPore 1.2**.

![demo image](docs/demo1.png)


## License

This project is released under the [Apache 2.0 license](LICENSE).

## Benchmark and model zoo

Results and models are available in the [model zoo](docs/model_zoo.md).

Supporting backbones:

- [x] [AlexNet]
- [x] [CPM]
- [x] [Hourglass]
- [x] [HRNet]
- [x] [MobilenetV2]
- [x] [ResNet]
- [x] [ResNetV1D]
- [x] [ResNext]
- [x] [SCNet]
- [x] [SEResNet]
- [x] [ShufflenetV1]

Supported methods for human pose estimation:

- [x] [CPM]
- [x] [SimpleBaseline]
- [x] [HRNet]
- [x] [Hourglass]
- [x] [SCNet]
- [x] [HigherHRNet]
- [x] [DarkPose]

Supported datasets:

- [x] [COCO](http://cocodataset.org/)
- [x] [MPII](http://human-pose.mpi-inf.mpg.de/)
- [x] [MPII-TRB](https://github.com/kennymckormick/Triplet-Representation-of-human-Body)
- [x] [AI Challenger](https://github.com/AIChallenger/AI_Challenger_2017)
- [x] [OCHuman](https://github.com/liruilong940607/OCHumanApi)
- [x] [CrowdPose](https://github.com/Jeff-sjtu/CrowdPose)
- [x] [OneHand10K](https://www.yangangwang.com/papers/WANG-MCC-2018-10.html)

## Feedbacks and Contact

The dynamic version is still under development, if you find any issue or have an idea on new features, please don't hesitate to contact us via [Gitee Issues](https://gitee.com/mindspore/mindvision/issues).

## Contributing

We appreciate all contributions to improve MindSpore Segmentation. Please refer to [CONTRIBUTING.md](./CONTRIBUTING.md) for the contributing guideline.

## Contributors

ZOMI(chenzomi12@gmail.com)
xiexingwang(1570475816@qq.com)
weihao(1920495491@qq.com)
hu-zhaoqiang(2467868073@qq.com)
OwenLee666(51775540@qq.com)
hansaifei(453751375@qq.com)
ljj(1309968475@qq.com)
Miooo(214149484@qq.com)
chen-chujia(2350422432@qq.com)
jianxu(864753683@qq.com)
zhong-shanji(935494217@qq.com)
hhf(879430198@qq.com)
HandSomEiM(houshuai319@gmail.com)
li-ying_a(1026312089@qq.com)
huangweijun(gdut_hwj_1@163.com)
CoDoJD(1685497601@qq.com)
M-Zif（lucas011014@qq.com）
lisa(xinyi.lisa.chen@gmail.com)
xudongliang(939369646@qq.com)
chen jiayang(744147327@qq.com)
cenfeijing（1521313560@qq.com)
lidong(lidon6@163.com)
cyf2054(1743922928@qq.com)
mindspore8188(1070882265@qq.com)
taxi110(2570300140@qq.com）
Lsr（1159592975@qq.com）
Xinyue Miao(1114558582@qq.com)
pallidlight(965504148@qq.com)
bridge(1131156501@qq.com)
chen-pengfei00(1012673739@qq.com)

jyf (1055818475@qq.com)
yinghuiwu（1639779591@qq.com）
lixiaoyu(1229282331@qq.com)
huang_zipeng（470612432@qq.com）
llxllxll（2233757511@qq.com）
ARIOZZ(1471566731@qq.com)
WuJianhui（1091538455@qq.com）
Lion(2646544390@qq.com)
hong_xin_ying(1241449350@qq.com)
YiYun(1493502917@qq.com)
zzz(1608773052@qq.com)
llxllxll(2233757511@qq.com)
Kylin- Aurora(207537969@qq.com)
zxr(1727787643@qq.com)
YxXian(850615324@qq.com)
ljz(957794003@qq.com)
qiulinwei(2320290936@qq.com)
zcf(605694021@qq.com)
## Acknowledgement

MindVision is an open source project that welcome any contribution and feedback. We wish that the toolbox and benchmark could serve the growing research community by providing a flexible as well as standardized toolkit to reimplement existing methods and develop their own new pose estimation methods.
